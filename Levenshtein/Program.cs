﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levenshtein
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1, s2;
            Console.Write("Please input the first string: ");
            s1 = Console.ReadLine();
            Console.Write("Please input the second string: ");
            s2 = Console.ReadLine();
            int n = s1.Length + 1;
            int m = s2.Length + 1;

            int[,] distance = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                distance[i, 0] = i;
            }

            for (int i = 0; i < m; i++)
            {
                distance[0, i] = i;
            }
            int dif;
            for (int i = 1; i < n; i++)
            {
                for (int j = 1; j < m; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                    {
                        dif = 0;
                    }
                    else
                    {
                        dif = 2;
                    }
                    int[] mins = { distance[i - 1, j] + 1, distance[i, j - 1] + 1, distance[i - 1, j - 1] + dif };
                    distance[i, j] = mins.Min();

                    if (i == 1 && j == 1)
                    {
                        Console.WriteLine("dif = {0}, s1 = {1}, s2 = {2}, min = {3}", dif, s1[0], s2[0], mins.Min());
                    }
                }
            }

            Console.WriteLine("Let's check the table");

            for (int i = n - 1; i >= 0; i--)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(distance[i, j] + " ");
                }
                Console.WriteLine();
            }

        }
    }
}
