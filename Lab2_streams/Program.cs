﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_streams
{
    class Program
    {
        static string path = @"D:\Games\Age of Empires II HD";
        static void ShowDirectory(DirectoryInfo dir)
        {
            DirectoryInfo[] subdirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine(files[i].Name);
            }
            Console.WriteLine("~~~Finished processing folder~~~" + Environment.NewLine);
            for (int i = 0; i < subdirs.Length; i++)
            {
                ShowDirectory(subdirs[i]);
            }
        }

        static void WatcherChanged(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("There were some changes to: {0}", e.FullPath);
        }

        static void Main(string[] args)
        {
            Task4();
        }

        public static void Task1()
        {
            DirectoryInfo dir = new DirectoryInfo(@"D:\Games\Age of Empires II HD");
            ShowDirectory(dir);
        }

        public static void Task2()
        {
            FileSystemWatcher watcher = new FileSystemWatcher(@"D:\Games\Age of Empires II HD");
            watcher.Filter = "*.docx";
            watcher.IncludeSubdirectories = true;
            watcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.Size;
            watcher.Changed += new FileSystemEventHandler(WatcherChanged);
            watcher.EnableRaisingEvents = true;
            while (true)
            {
                int a = 1;
            }
            //watcher.WaitForChanged(WatcherChangeTypes.All);
        }

        public static void Task3()
        {
            string[] myFriends = new string[] { "Danil", "Sattar", "Dana", "Aidana" };
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < myFriends.Length - 1; i++)
            {
                sb.Append(myFriends[i] + ", ");
            }
            sb.Append(myFriends[myFriends.Length - 1] + ".");

            // Запись в файл
            FileStream fs = new FileStream("data.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(sb);
            sw.Close();
            fs.Close();

            // Чтение из файла
            List<string> friends = new List<string>();
            using (StreamReader reader = new StreamReader("data.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    friends.AddRange(line.Split(new string[] { " ", ",", "."}, StringSplitOptions.RemoveEmptyEntries));
                }
            }
            foreach (var friend in friends)
            {
                Console.WriteLine("- {0}", friend);
            }

            // Опять запись
            string[] newFriends = new string[] { "Loli", "Boli" };
            using (StreamWriter writer = File.AppendText("data.txt"))
            {
                for (int i = 0; i < newFriends.Length; i++)
                {
                    writer.Write(newFriends[i] + ",");
                }
            }

        }

        public static void Task4()
        {
            Directory.CreateDirectory(@"D:\Games\Age of Empires II HD\compressed");
            Directory.CreateDirectory(@"D:\Games\Age of Empires II HD\deCompressed");

            DirectoryInfo dir = new DirectoryInfo(@"D:\Games\Age of Empires II HD");
            FileInfo[] files = dir.GetFiles();
            int b;

            for (int i = 0; i < files.Length; i++)
            {
                FileStream sourceFile = File.OpenRead(files[i].FullName);
                FileStream destFile = File.Create(@"D:\Games\Age of Empires II HD\compressed\" + files[i].Name +".gzip");
                GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
                b = sourceFile.ReadByte();
                while (b != -1)
                {
                    compStream.WriteByte((byte)b);
                    b = sourceFile.ReadByte();
                }
                Console.WriteLine("Compressing file: " + files[i].Name);
                compStream.Close();
                destFile.Close();
                sourceFile.Close();
            }
            Console.WriteLine();

            for (int i = 0; i < files.Length; i++)
            {
                FileStream decompressedFile = File.Create(@"D:\Games\Age of Empires II HD\deCompressed\" + files[i].Name);
                FileStream compressedFile = File.OpenRead(@"D:\Games\Age of Empires II HD\compressed\" + files[i].Name + ".gzip");

                GZipStream decompStream = new GZipStream(compressedFile, CompressionMode.Decompress);
                b = decompStream.ReadByte();
                while (b != -1)
                {
                    decompressedFile.WriteByte((byte)b);
                    b = decompStream.ReadByte();
                }
                Console.WriteLine("DeCompressing file: " + files[i].Name);
                decompStream.Close();
                compressedFile.Close();
                decompressedFile.Close();
            }

        }
    }
}
