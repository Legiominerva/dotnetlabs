﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs
{
    enum Genders
    {
        Male, 
        Female
    }
    class SortString
    {
        public string text;

        public SortString(string input)
        {
            this.text = input;
        }

        public string Sort()
        {
            string[] arr = text.Split();
            Array.Sort(arr);
            return String.Join(" ", arr);
        }
    }
    class Person
    {
        public string firstName;
        public string lastName;
        public int age;
        public Genders gender;

        public Person(string fName, string lName, int age)
        {
            this.firstName = fName;
            this.lastName = lName;
            this.age = age;
        }

        public Person(string fName, string lName, int age, Genders gen): this(fName, lName, age)
        {
            this.gender = gen;
        }

        public override string ToString()
        {
            return "First Name: " + firstName + ", Last Name: " + lastName + ", age: " + age + ", gender: " + gender + ".\t";
        }
    }
    class Manager: Person
    {
        public string phoneNumber;
        public string officeLocation;

        public Manager(string fName, string lName, int age, string phone, string location): base(fName, lName, age)
        {
            this.phoneNumber = phone;
            this.officeLocation = location;
        }

        public Manager(string fName, string lName, int age, Genders gen, string phone, string location): 
            this(fName, lName, age, phone, location)
        {
            this.gender = gen;
        }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + "This is a manager whose phone number is " + phoneNumber + " and office locations is " + officeLocation + Environment.NewLine;
            //return "First Name: " + firstName + ", Last Name: " + lastName + ", age: " + age + ", gender: " + gender + "." + Environment.NewLine + "This is a manager whose phone number is " + phoneNumber + " and office locations is " + officeLocation + Environment.NewLine;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            // Task 1 - Person class
            //Task1();
            // Task 2 - Sorting a string
            //Task2();
            // Task 3 - Exception handling
            //Task3();
            // Task 4 - Inheritance of Manager class from Person class
            Task4();
            // Task 5 - Implicit type conversion
            //Task5();
        }
        public static void Task1()
        {
            Person p1 = new Person("Ivan", "Ivanov", 24, Genders.Male);
            Console.WriteLine(p1);
            // why it assumes gender for this constructor???
            Console.WriteLine(new Person("Hugh", "Laurie", 56));
        }
        public static void Task2()
        {
            SortString s1 = new SortString("I am just a string, don't mind me please");
            Console.WriteLine(s1.Sort());
        }
        public static void Task3()
        {
            string fileName;
            Console.Write("Please enter file name: ");
            fileName = Console.ReadLine();
            try
            {
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                TextReader tr = new StreamReader(fs);
                try
                {
                    //StreamReader sr = new StreamReader(fs);
                    string text = tr.ReadToEnd();
                    Console.WriteLine(text);
                    Console.WriteLine("File has been read successfully!");
                }
                catch (Exception e)
                {
                    if (e is UnauthorizedAccessException)
                    {
                        Console.WriteLine("Error occured when accessing the file. Access is denied.");
                    }
                    else
                    {
                        throw;
                    }
                }
                finally
                {
                    tr.Close();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                if (e is FileNotFoundException)
                {
                    Console.WriteLine("The file does not exist!");
                }
                else
                {
                    throw;
                }
                
            }
            
            
        }
        public static void Task4()
        {
            Manager m1 = new Manager("Bob", "Dylan", 45, "+77778547799", "Almaty, Bogenbai street 56");
            Console.WriteLine(m1);
        }
        public static void Task5()
        {
            Int16 i16 = 8;
            Int32 i32 = 50;
            double d = 3.14159;

            //i16 = i32;
            //i16 = d;

            i32 = i16;
            //i32 = d;

            i32 = 90;
            d = i16;
            Console.WriteLine(d);
            d = i32;
            Console.WriteLine(d);
        }
    }
}
