﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;

namespace FARmanager.Classes
{
    static class Helper
    {
        [DllImport("kernel32.dll", ExactSpelling = true)]

        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;

        public static void InitConsole()
        {
            ShowWindow(ThisConsole, MAXIMIZE);
        }

        public static Bitmap Resize(Bitmap originalBitmap, int maxHeight, int maxWidth)
        {
            int width = originalBitmap.Width;
            int height = originalBitmap.Height;

            //while (width > maxWidth || height > maxHeight)
            //{
            //    width /= 2;
            //    height /= 2;
            //    scale++;
            //}
            double maxAverage;
            double p1, p2, p3, p4;
            Color c1, c2, c3, c4, goodColor = Color.White;
            Bitmap temp = originalBitmap;
            Bitmap resizedImage = originalBitmap;
            while (width > maxWidth || height > maxHeight)
            {
                resizedImage = new Bitmap(width / 2, height / 2);
                for (int row = 0; row < height - 1; row += 2)
                {
                    for (int col = 0; col < width - 1; col += 2)
                    {
                        c1 = temp.GetPixel(col, row);
                        p1 = (c1.R + c1.G + c1.B) / 3;

                        c2 = temp.GetPixel(col, row + 1);
                        p2 = (c2.R + c2.G + c2.B) / 3;

                        c3 = temp.GetPixel(col + 1, row);
                        p3 = (c3.R + c3.G + c3.B) / 3;

                        c4 = temp.GetPixel(col + 1, row + 1);
                        p4 = (c4.R + c4.G + c4.B) / 3;

                        maxAverage = Math.Max(Math.Max(p1, p2), Math.Max(p3, p4));
                        

                        if (maxAverage == p1)
                        {
                            goodColor = c1;
                        }
                        else if (maxAverage == p2)
                        {
                            goodColor = c2;
                        }
                        else if (maxAverage == p3)
                        {
                            goodColor = c3;
                        }
                        else if (maxAverage == p4)
                        {
                            goodColor = c4;
                        }

                        resizedImage.SetPixel(col / 2, row / 2, goodColor);
                    }
                }
                temp = resizedImage;
                width /= 2;
                height /= 2;
            }
            return resizedImage;
        }

        public static ConsoleColor ClosestConsoleColor(byte r, byte g, byte b)
        {
            ConsoleColor returnConsoleColor = 0;
            double red = r, green = g, blue = b, minDistance = double.MaxValue;

            foreach (ConsoleColor consoleColor in Enum.GetValues(typeof(ConsoleColor)))
            {
                var ccName = Enum.GetName(typeof(ConsoleColor), consoleColor);
                var color = System.Drawing.Color.FromName(ccName == "DarkYellow" ? "Orange" : ccName); // bug fix
                var distance = Math.Pow(color.R - red, 2.0) + Math.Pow(color.G - green, 2.0) + Math.Pow(color.B - blue, 2.0);
                if (distance == 0.0)
                    return consoleColor;
                if (distance < minDistance)
                {
                    minDistance = distance;
                    returnConsoleColor = consoleColor;
                }
            }
            return returnConsoleColor;
        }

        //public static ConsoleColor RGBtoConsoleColor(Color rgbcolor)
        //{

        //    //ConsoleColor bestMatch = ConsoleColor.White;
        //    //int bestSqrDist = Int32.MaxValue;
        //    //ConsoleColor[] consoleColors = new[] {
        //    //    ConsoleColor.Black,
        //    //    ConsoleColor.Red
        //    //};
        //    //foreach (ConsoleColor consoleColor in consoleColors)
        //    //{
        //    //    int sqrDist = Sqr(rgbcolor.R - consoleColor.) + Sqr(rgbcolor.G - consoleColor.G) + Sqr(rgbcolor.B - consoleColor.B);
        //    //    if (sqrDist < bestSqrDist)
        //    //    {
        //    //        bestMatch = consoleColor;
        //    //        bestSqrDist = sqrDist;
        //    //    }
        //    //}
        //    //var map = new Dictionary<Color, ConsoleColor>();
        //    //map[Color.Red] = ConsoleColor.Red;
        //    //map[Color.Blue] = ConsoleColor.Blue;

        //    //return map[bestMatch.Name];
        //}
    }


    //public static void Main()
    //{
    //    ConsoleKeyInfo cki;
    //    // Prevent example from ending if CTL+C is pressed.
    //    Console.TreatControlCAsInput = true;

    //    Console.WriteLine("Press any combination of CTL, ALT, and SHIFT, and a console key.");
    //    Console.WriteLine("Press the Escape (Esc) key to quit: \n");
    //    do
    //    {
    //        cki = Console.ReadKey();
    //        Console.Write(" --- You pressed ");
    //        if ((cki.Modifiers & ConsoleModifiers.Alt) != 0) Console.Write("ALT+");
    //        if ((cki.Modifiers & ConsoleModifiers.Shift) != 0) Console.Write("SHIFT+");
    //        if ((cki.Modifiers & ConsoleModifiers.Control) != 0) Console.Write("CTL+");
    //        Console.WriteLine(cki.Key.ToString());
    //    } while (cki.Key != ConsoleKey.Escape);
    //}
}
