﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARmanager.Classes
{
    enum State
    {
        OpenedTextFile, 
        OpenedImageFile, 
        ShowItems
    }
    class FARwindow
    {
        private List<FileSystemInfo> items;
        private string currentDirectoryPath;
        private DirectoryInfo currentDirectory;
        private long currentDirectorySize;
        private int currentIndex;
        private int prevIndex;

        private int pageOffset;
        private int pageSize;

        private State state;

        private Point indent;

        // Координаты начала окна FAR'a
        public Point origin;

        public int Width { get; }
        public int Height { get; }

        public ConsoleColor BackColor { get; }
        public ConsoleColor ForeColor { get; set; }
        public ConsoleColor BorderColor { get; }

        private bool run;
        public bool GetRun()
        {
            return run;
        }

        public FARwindow(): this(new Point(), 100, 500, ConsoleColor.Blue, ConsoleColor.White, ConsoleColor.Cyan)
        {

        }

        public FARwindow(Point p, int w, int h, ConsoleColor back, ConsoleColor fore, ConsoleColor border)
        {
            origin = new Point(p.X, p.Y);
            Width = w;
            Height = h;
            BackColor = back;
            ForeColor = fore;
            BorderColor = border;
            currentDirectorySize = 0;

            prevIndex = 0;
            currentIndex = 0;

            pageOffset = 0;
            // пока 15. Надо подумать, хороший ли это метод
            pageSize = Height - 15;

            items = new List<FileSystemInfo>();
            currentDirectoryPath = @"D:\Games\Age of Empires II HD";

            ChangeDirectory(currentDirectoryPath, 0);

            run = true;
            state = State.ShowItems;

            indent = new Point(origin.X + 2, origin.Y + 3);
        }

        public void ChangeDirectory(string path, int index)
        {
            currentDirectoryPath = path;
            currentDirectory = new DirectoryInfo(currentDirectoryPath);
            items.Clear();
            // пока временное решение
            // по хорошему должно стирать рабочую область
            ClearWorkRegion();
            DrawBorders();
            // Для ".."
            if (currentDirectory.Parent != null)
            {
                items.Add(currentDirectory.Parent);
            }
            items.AddRange(currentDirectory.GetDirectories());
            items.AddRange(currentDirectory.GetFiles());
            currentDirectorySize = 0;
            foreach (var file in currentDirectory.GetFiles())
            {
                currentDirectorySize += file.Length;
            }
            currentIndex = index;
            DrawMainFolderInfo();
        }

        public void KeyPress(ConsoleKeyInfo pressedKey)
        {
            switch (pressedKey.Key)
            {
                case ConsoleKey.UpArrow:
                    if ((currentIndex > 0) && (state == State.ShowItems)) currentIndex--;
                    break;
                case ConsoleKey.DownArrow:
                    if ((currentIndex < items.Count - 1) && (state == State.ShowItems)) currentIndex++;
                    break;
                case ConsoleKey.Enter:
                    //если это файл
                    if (items[currentIndex].GetType() == typeof(FileInfo))
                    {
                        // надо доделать OpenedTextFile
                        state = State.OpenedImageFile;
                        // картинка
                        ShowImage(items[currentIndex].FullName);
                    }
                    //если это папка
                    if (items[currentIndex].GetType() == typeof(DirectoryInfo))
                    {
                        // возвращаемся в родительскую папку
                        if (currentIndex == 0)
                        {
                            ChangeDirectory(items[currentIndex].FullName, prevIndex);
                            prevIndex = 0;
                        }
                        // заходим глубже
                        else
                        {
                            prevIndex = currentIndex;
                            ChangeDirectory(items[currentIndex].FullName, 0);
                        }
                    }

                    break;
                case ConsoleKey.Escape:

                    //Выходит в родительскую папку, если не открыт файл
                    if (state == State.ShowItems)
                    {
                        if (currentDirectory.Parent != null)
                        {
                            ChangeDirectory(currentDirectory.Parent.FullName, prevIndex);
                        }
                    }
                    // Открыт файл
                    if (state == State.OpenedImageFile)
                    {
                        state = State.ShowItems;
                        ClearWorkRegion();
                        DrawBorders();
                        Draw();
                    }
                    break;
                case ConsoleKey.X:
                    run = false;
                    break;
            }
        }

        public void ShowImage(string path)
        {
            Console.Clear();
            Bitmap image = new Bitmap(path);
            Bitmap resizedImage = Helper.Resize(image, Console.BufferHeight - 1, Console.BufferWidth - 1);
            for (int row = 0; row < resizedImage.Height; row++)
            {
                for (int col = 0; col < resizedImage.Width; col++)
                {
                    var cc = Helper.ClosestConsoleColor(resizedImage.GetPixel(col, row).R, resizedImage.GetPixel(col, row).G, resizedImage.GetPixel(col,row).B);
                    Console.ForegroundColor = cc;
                    Console.Write((char)9608);
                }
                Console.Write(Environment.NewLine);
            }

            
            //Console.Clear();
            //Bitmap image = new Bitmap(path);
            //for (int col = 0; col < image.Width; col++)
            //{
            //    for (int row = 0; row < image.Height; row++)
            //    {
            //        Console.ForegroundColor = (ConsoleColor)(int)Helper.RGBtoConsoleColor(image.GetPixel(row, col));
            //        Console.Write((char)9608);
            //    }
            //}
        }

        public void Draw()
        {
            Console.BackgroundColor = BackColor;
            if (state == State.ShowItems)
            {
                // Временно 
                // Console.Clear();
                // Нельзя выходить за PageSize при прокрутке
                int i = 0;
                if (currentIndex > pageOffset + pageSize) pageOffset++;
                if (currentIndex < pageOffset) pageOffset--;
                i = pageOffset;
                // Костыль :D
                //if (i != 0)
                //{
                //    Console.Clear();
                //}

                for (; i < items.Count && i < pageOffset + pageSize + 1; i++)
                {
                    bool isFolder = true;
                    // Подефолту это папка
                    if (i == currentIndex)
                    {
                        DrawInfo(items[i], isFolder);
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    // Файл
                    if (items[i].GetType() == typeof(FileInfo))
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        isFolder = false;
                    }
                    //Console.SetCursorPosition(indent.X, indent.Y + i - pageOffset);

                    //Самая 1ая строка ".."
                    if (currentDirectory.Parent != null && i == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        DrawRow("..", indent.X, indent.Y + i - pageOffset);
                        //Console.Write("..");
                    } 
                    else
                    {
                        //Console.Write(items[i].Name);
                        DrawRow(items[i].Name, indent.X, indent.Y + i - pageOffset);
                        //Console.Write("\t\t\t\t");
                        //Console.Write(items[i].CreationTime);
                    }

                    ResetColors();
                }
            }
        }

        public void DrawRow(string s, int x, int y)
        {
            Console.SetCursorPosition(x, y);
            string name = s;
            // для длинных названий
            if (s.Length + x > Console.BufferWidth / 2 - 1)
            {
                name = s.Substring(0, Console.BufferWidth / 2 - 4 - x);
                name += "...";
            }
            
            while (name.Length + x < Console.BufferWidth / 2 - 1)
            {
                name += " ";
            }
            Console.Write(name);
            // Чтобы border не затирал
            Console.SetCursorPosition(x, y + 2);
        }

        public void DrawMainFolderInfo()
        {
            Console.SetCursorPosition(indent.X, Console.BufferHeight - indent.Y - 3);
            Console.Write("Bytes: {0}", currentDirectorySize.ToString());
        }

        public void DrawInfo(FileSystemInfo fsi, bool isFolder)
        {
            Console.SetCursorPosition(indent.X, Console.BufferHeight - indent.Y - 4);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = BackColor;
            string info = fsi.Name;
            //DrawRow(name, indent.X, Console.BufferHeight - indent.Y - 4);
            // для длинных названий
            if (fsi.Name.Length + indent.X + fsi.CreationTime.ToString().Length > Console.BufferWidth / 2 - 1)
            {
                info = fsi.Name.Substring(0, Console.BufferWidth / 2 - 5 - fsi.CreationTime.ToString().Length - indent.X);
                info += "...";
            }

            while (info.Length + indent.X + fsi.CreationTime.ToString().Length < Console.BufferWidth / 2 - 1)
            {
                info += " ";
            }
            info += fsi.CreationTime.ToString();
            Console.Write(info);            
        }

        public void ResetColors()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = ForeColor;
        }

        public void ClearWorkRegion()
        {
            Console.BackgroundColor = BackColor;
            Console.Clear();
        }

        public void DrawBorders()
        {
            DrawTopBorder();
            DrawRightBorder();
            DrawBottomBorder();
            DrawLeftBorder();
        }

        public void DrawTopBorder()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = BorderColor;

            Console.SetCursorPosition(origin.X, origin.Y);
            Console.Write("+");
            for (int i = 1; i < Width - 1; i++)
            {
                Console.Write("-");
            }
            Console.Write("+");
        }

        public void DrawBottomBorder()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = BorderColor;
            Console.SetCursorPosition(origin.X, origin.Y + Height - 1);
            Console.Write("+");
            for (int i = 1; i < Width - 1; i++)
            {
                Console.Write("-");
            }
            Console.Write("+");
        }

        public void DrawLeftBorder()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = BorderColor;

            for (int i = 1; i < Height - 1; i++)
            {
                Console.SetCursorPosition(origin.X, origin.Y + i);
                Console.Write("|");
            }
        }

        public void DrawRightBorder()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = BorderColor;

            for (int i = 1; i < Height - 1; i++)
            {
                Console.SetCursorPosition(origin.X + Width - 1, origin.Y + i);
                Console.Write("|");
            }
        }

        public void DrawAll()
        {
            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = BorderColor;
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.SetCursorPosition(origin.X + j, origin.Y + i);
                    Console.Write("*");
                }
            }
        }
    }
}
