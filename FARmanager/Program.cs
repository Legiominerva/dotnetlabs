﻿using FARmanager.Classes;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace FARmanager
{
    class Program
    {

        // Helpers

        // Opern file with default app
        // System.Diagnostics.Process.Start(@"D:\Downloads\Torrent\Jimi Hendrix - The Greatest Hits 2CD [Bubanee]\CD2\01 - Stone Free.mp3");




        static void Main(string[] args)
        {
            Console.SetBufferSize(Console.LargestWindowWidth - 1, Console.LargestWindowHeight - 1);
            //Console.WriteLine(Console.LargestWindowHeight);
            //Console.WriteLine(Console.LargestWindowWidth);
            //Console.WriteLine(Console.BufferHeight);
            //Console.WriteLine(Console.BufferWidth);

            Helper.InitConsole();

            //Console.ReadLine();
            //Console.WriteLine("hehe");

            FARwindow w1 = new FARwindow(new Point(), Console.BufferWidth / 2, Console.BufferHeight, ConsoleColor.Blue, ConsoleColor.White, ConsoleColor.Cyan);
            FARwindow w2 = new FARwindow(new Point(Console.BufferWidth / 2, 0), Console.BufferWidth / 2, Console.BufferHeight, ConsoleColor.Yellow, ConsoleColor.White, ConsoleColor.DarkGreen);

            w1.ClearWorkRegion();
            w1.DrawBorders();
            // Blinking of cursor
            Console.CursorVisible = false;
            while (w1.GetRun())
            {
                w1.Draw();

                ConsoleKeyInfo pressedKey = Console.ReadKey();
                w1.KeyPress(pressedKey);
                
            }
            
            w2.DrawAll();

            //byte[] bytes = new byte[] { 0x00002015 };

            //string unicodeString = Encoding.UTF32.GetString(c);

            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = ConsoleColor.Black;


            Console.ReadKey();
        }
    }
}
