﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabLevenshtein
{
    public partial class Form1 : Form
    {
        private List<string> text;
        private List<string> dict;
        private char[] delims;
        private Dictionary<string, string[]> corrections;
        private string selectedWord;
        private int selStart;
        private int selFinish;

        public Form1()
        {
            InitializeComponent();
            text = new List<string>();
            delims = new char[] { ' ', ',', '.', ':', ';', '?' };
            corrections = new Dictionary<string, string[]>();
            selectedWord = "";

            try
            {
                FileStream fs = new FileStream("words.dat", FileMode.Open, FileAccess.Read);
                TextReader tr = new StreamReader(fs);

                dict = tr.ReadToEnd().Split().ToList();

                tr.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                richBox.Text = "There was an error when opening stream";
            }
            
        }


        private void CheckAll(object sender, EventArgs e)
        {
            
            List<string> newList = richBox.Text.Split(delims, StringSplitOptions.RemoveEmptyEntries).ToList();

            foreach (string word in newList)
            {
                if (!dict.Contains(word))
                {
                    Highlight(word, Color.Red);
                }
                else
                {
                    Highlight(word, Color.Black);
                }
            }
        }

        private void Highlight (string word, Color color)
        {
            if (word == string.Empty) return;

            int s_start = richBox.SelectionStart;
            int startIndex = 0;
            int index;

            while((index = richBox.Text.IndexOf(word, startIndex)) != -1) 
            {
                richBox.Select(index, word.Length);
                richBox.SelectionColor = color;

                startIndex = index + word.Length;
            }

            richBox.SelectionStart = s_start;
            richBox.SelectionLength = 0;
            richBox.SelectionColor = Color.Black;
        }

        private void richBox_TextChanged(object sender, EventArgs e)
        {
            // Work with text only when 'space' is press
            //  MessageBox.Show(""+richBox.SelectionStart);
            if (richBox.TextLength == 0) return;
            if (delims.Contains(richBox.Text[richBox.TextLength - 1]))
            {
                //correctionBox.Text += "space ";
                CheckAll(this, e);

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void richBox_SelectionChanged(object sender, EventArgs e)
        {
            if (richBox.SelectedText == "" && richBox.SelectionStart != richBox.TextLength && richBox.SelectionColor == Color.Red)
            {
                //label3.Text = "start = " + richBox.SelectionStart.ToString() + ", lenght = " + richBox.TextLength.ToString();
                int sel = richBox.SelectionStart;
                int index = 0;
                selectedWord = "";

                // select to the right
                if (sel + index < richBox.TextLength)
                {
                    while (sel + index < richBox.TextLength && !delims.Contains(richBox.Text[sel + index]))
                    {
                        selectedWord += richBox.Text[sel + index];
                        index++;
                    }
                }

                selFinish = sel + index - 1;
                

                // select to the left
                index = -1;
                if (sel + index >= 0)
                {
                    while (sel + index >= 0 && !delims.Contains(richBox.Text[sel + index]))
                    {
                        selectedWord = richBox.Text[sel + index].ToString() + selectedWord;
                        index--;
                    }
                }
                selStart = sel + index + 1;
                
                string[] hints = Hint(selectedWord);

                comboBox.Items.Clear();

                for (int i = 0; i < 5; i++)
                {
                    comboBox.Items.Add(hints[i]);
                }
                comboBox.SelectedIndex = 0;
                
            }
        }

        private string[] Hint(string word)
        {
            if (corrections.ContainsKey(word))
            {

            }
            else
            {
                // need to add this word to corrections dictionary
                List<int> distances = new List<int>();
                foreach (var correctWord in dict)
                {
                    distances.Add(Levenstein(correctWord, word));
                }
                // add words to corrections
                // start with min distance

                int index, added = 0;
                int startIndex = 0;
                corrections[word] = new string[5];

                for (int i = 0; i < 6; i++)
                {
                    startIndex = 0;
                    while ((index = distances.IndexOf(distances.Min() + i, startIndex)) != -1 && added < 5)
                    {
                        corrections[word][added] = dict[index];
                        added++;
                        startIndex = index + 1;
                    }
                }
            }
            return corrections[word];
        }

        private void richBox_MouseClick(object sender, MouseEventArgs e)
        {

        }

        /// <summary>
        /// Calculate Levestein distance between two strings
        /// </summary>
        /// <param name="s1">First input string</param>
        /// <param name="s2">Second input string</param>
        /// <returns>Levenstein distance</returns>
        public int Levenstein(string s1, string s2)
        {
            int n = s1.Length + 1;
            int m = s2.Length + 1;
            int[,] distance = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                distance[i, 0] = i;
            }

            for (int i = 0; i < m; i++)
            {
                distance[0, i] = i;
            }
            int dif;
            for (int i = 1; i < n; i++)
            {
                for (int j = 1; j < m; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                    {
                        dif = 0;
                    }
                    else
                    {
                        dif = 2;
                    }
                    int[] mins = { distance[i - 1, j] + 1, distance[i, j - 1] + 1, distance[i - 1, j - 1] + dif };
                    distance[i, j] = mins.Min();
                }
            }
            return distance[n - 1, m - 1];
        }

        private void change_Click(object sender, EventArgs e)
        {
            if (comboBox.Items.Count != 0)
            {
                int s_start = richBox.SelectionStart;
                richBox.SelectionStart = selStart;
                richBox.SelectionLength = selFinish - selStart + 1;
                richBox.SelectedText = comboBox.SelectedItem.ToString();

                richBox.SelectionStart = selStart;
                richBox.SelectionLength = comboBox.SelectedItem.ToString().Length;
                richBox.SelectionColor = Color.Black;
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            if (selectedWord != "")
            {
                //label3.Text = selectedWord;
                dict.Add(selectedWord);
                CheckAll(sender, e);

                // Write to file
                try
                {
                    string path = @"words.dat";

                    if (File.Exists(path))
                    {
                        File.AppendAllText(path, selectedWord + Environment.NewLine);
                    }
                    else
                    {
                        MessageBox.Show("Could not append to file!");
                    }
                }
                catch (Exception ex)
                {
                    richBox.Text = "There was an error when opening stream";
                }
            }
            comboBox.Items.Clear();
        }
    }
}
